# Makefile for running the QA Framework

PYTHON = python3

QA_TOOLBOX_DIR = qa-toolbox

all : setup_doc_dirs run_tests build_documentation

setup_doc_dirs :
	@echo setting up documentation directories
	mkdir -p docs; cd docs; mkdir -p source

run_tests :
	@echo run_tests
	@if [ -d $(QA_TOOLBOX_DIR) ]; then \
		echo Executing QA Framework Makefile; \
		$(MAKE) --directory=$(QA_TOOLBOX_DIR) DOC_DIR=${PWD}/docs/source CONFIG_FILE=${PWD}/config_files.txt SIMULATORS_FILE=${PWD}/simulators.sim all; \
		echo QA Framework Makefile done; \
	else \
		echo Directory $(QA_TOOLBOX_DIR) not found; \
	fi

build_documentation :
	@echo build_documentation
	$(MAKE) --directory=docs html
