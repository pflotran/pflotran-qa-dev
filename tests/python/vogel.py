# Analytical solution in this script are based on:
# Vogel, Peter and Jobst MaBmann (2015) Chapter 2: Verification Tests in
# "Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
# Modelling and Benchmarking, Closed-Form Solutions", 
# Olaf Kolditz, Hua Shao, Wenqing Wang and Sebastian Bauer (eds.),
# Terrestrial Environment Sciences, 
# Springer International Publishing, Switzerland.

import sys
import os
import traceback

filename = __file__

import inspect
parent = inspect.getmodulename(inspect.stack()[-1][1])

try:
  qa_toolbox_dir = os.environ['QA_TOOLBOX_DIR']
except KeyError:
  print('QA_TOOLBOX_DIR must be set in order to use python '
        'script {}.'.format(filename))
  sys.exit(1)

sys.path.append(qa_toolbox_dir)
sys.path.append(qa_toolbox_dir + '/simulators')

import numpy as np
import math

from qa_solution import QASolutionWriter
from simulator_modules.python import *

epsilon_value = 1.e-30

def vogel_2_2_7(L=50.,num_cells=200):

# Section 2.2.7, pg.32 
# "A Transient 1D Pressure Distribution, Time-Dependent Boundary Conditions
# of 1st Kind"
#
# Author: Glenn Hammond
# Date: 2020-02-26
#
# Modifications from original problem:
#   p0(x,t) = 250,000 [Pa]

    print('Beginning Vogel 2.2.7')

#    L = 50.                     # [m]
#    num_cells = 200
    dx = 2.*L/num_cells          # [m]
    dy = 1.
    dz = 1.
    k = 1.0e-14                  # [m^2]
    mu = 1.728e-3                # [Pa*sec]
    phi = 0.25                   # [m^3 pore/m^3 bulk]
    kappa = 1.0e-9               # [1/Pa]
    p1_per_day = 2.0e6           # [Pa/day]
    p_offset = 0.25e6            # [Pa]
    chi = k/(phi*mu*kappa)       # [m^2/s]
    tunit = 'd'

    x = np.linspace(-L+0.5*dx,L-0.5*dx,num_cells)
    pressure = np.zeros(num_cells)                               # [Pa]
    times = np.array([0.10,0.25,0.50,0.75])                      # [day]

    solution_filename = get_python_solution_filename(parent)
    solution = QASolutionWriter(solution_filename,tunit)

    solution.write_coordinates(x,[0.5*dy],[0.5*dz])

    # create the analytical solution
    for t in times:
        sum_ = np.zeros(num_cells)
        max_delta = 1.0
        tconv = 24.*3600.
        seconds = t * tconv
        p1_per_sec = p1_per_day / tconv
        n = 0
        while max_delta > epsilon_value:
            delta = \
                 pow(-1.,n)/pow(2.*n+1,3)* \
                 np.cos(math.pi*x*(2.*n+1)/(2*L))* \
                 np.exp(-chi*pow(2.*n+1,2)*math.pi*math.pi*seconds/(4.*L*L))
            sum_ += delta
            max_delta = np.max(np.abs(delta))
            n = n + 1
        pressure[:] = p_offset + \
            p1_per_sec*seconds + p1_per_sec*(pow(x,2)-L*L)/(2.*chi) + \
            16.*p1_per_sec*L*L/(chi*pow(math.pi,3))*sum_
        print(t)
        solution.write_dataset(t,pressure,'Liquid Pressure')

    solution.destroy()

    print('Finishing Vogel 2.2.7')

    return True
